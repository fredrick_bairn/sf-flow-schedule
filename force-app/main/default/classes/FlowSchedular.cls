global without sharing class FlowSchedular implements Schedulable, Database.Batchable<sObject> {
    @InvocableMethod(label = 'Schedule Flow' description = 'Schedules a flow to run in the future.' category = 'Scheduled')
    public static List<Response> scheduleFlow(List<Requests> requestList) {
        List<Response> responseList = new List<Response>();

        Map<String, Scheduled_Flow__c> exsistingSchedules = getExistingSchedules(requestList);

        List<Scheduled_Flow__c> scheduledFlows = new List<Scheduled_Flow__c>();
        for (Requests req : requestList) {
            try {
                Flow.Interview flowInstance = Flow.Interview.createInterview(req.flowApi, new Map <String, Object>());
            } catch (TypeException ex) {
                throw new FlowException('The flow ' + req.flowApi + ' does not exist.');
            }

            Integer addMinutes = Integer.valueOf(req.hours * 60);
            DateTime runTime = nextQuaterHour(DateTime.now().addMinutes(addMinutes));
            Scheduled_Flow__c newSchedule = new Scheduled_Flow__c(
                Name = req.flowApi + ' ' + req.recordId,
                Flow_API_Name__c = req.flowApi,
                Record_Id__c = req.recordId,
                Run_Time__c = runTime,
                Status__c = 'Pending',
                Notes__c = ''
                );
            if(exsistingSchedules.containsKey(req.flowApi + ' ' + req.recordId)) {
                newSchedule.Id = exsistingSchedules.get(req.flowApi + ' ' + req.recordId).Id;
            }
            scheduledFlows.add(newSchedule);
            responseList.add(new Response(newSchedule));
        }
        upsert scheduledFlows;
        return responseList;
    }

    SchedulableContext sc;
    public FlowSchedular(SchedulableContext sc){
        this.sc = sc;
    }

    public FlowSchedular(){
    }

    global void execute(SchedulableContext SC) {
        Database.executeBatch(new FlowSchedular(sc), 1);
    }

    public static void callFlow(String flowName, Id recordId) {
        Map <String, Object> inputs = new Map <String, Object>();
        inputs.put('recordId', recordId);
        try {
            Flow.Interview flowInstance = Flow.Interview.createInterview(flowName, inputs);
            flowInstance.start();
        } catch (TypeException ex) {
            throw new FlowException('The flow ' + flowName + ' does not exist.');
        }
    }

    //--- Batchable Methods---
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, Flow_API_Name__c, Record_Id__c, Run_Time__c, Status__c
                                         FROM Scheduled_Flow__c
                                         WHERE Status__c = 'Pending' AND Run_Time__c <= : system.now()]);
    }

    public void execute(Database.BatchableContext BC, List<Scheduled_Flow__c> scope){
        for(Scheduled_Flow__c s : scope) {
            try {
                callFlow(s.Flow_API_Name__c, s.Record_Id__c);
                s.Status__c = 'Completed';
            } catch (Exception ex) {
                s.Status__c = 'Error';
                s.Notes__c = ex.getMessage() + '\r\n' + ex.getStackTraceString();
            }
        }
        update scope;
    }

    public void finish(Database.BatchableContext BC){
        Delete [SELECT Id, Name, Flow_API_Name__c, Record_Id__c, Run_Time__c, Status__c
                FROM Scheduled_Flow__c
                WHERE Status__c = 'Completed'];
        nextRun();
    }
    //---End Batchable Methods ---/

    public void nextRun(){
        // Abort the current job
        if(sc != null) {
            Id jobId = sc.getTriggerId();
            System.abortJob(jobId);
        }

        DateTime nextRunTime = nextQuaterHour(DateTime.now());

        String cronString = '0 ' + nextRunTime.minute() + ' ' +
                            nextRunTime.hour() + ' ' + nextRunTime.day() + ' ' +
                            nextRunTime.month() + ' ? ' + nextRunTime.year();


        System.schedule(FlowSchedular.class.getName() + '-' + nextRunTime.format(), cronString, new FlowSchedular());
    }

    private static DateTime nextQuaterHour(DateTime current){
        DateTime nextRunTime = current;
        //Convert to quater hours;
        Integer nextQuater = Integer.valueOf((Math.floor(nextRunTime.minute() / 15) + 1) * 15);
        Integer nextMinute = math.mod(nextQuater, 60);

        //Account for moving to the next hour.
        if(nextRunTime.minute() > 44) {
            nextRunTime = nextRunTime.addHours(1);
        }

        return DateTime.newInstance(nextRunTime.year(), nextRunTime.month(), nextRunTime.day(), nextRunTime.hour(), nextMinute, 0);
    }

    private static Map<String, Scheduled_Flow__c> getExistingSchedules(List<Requests> requestList){
        Set<String> scheduleNames = new Set<String>();
        for (Requests req : requestList) {
            scheduleNames.add(req.flowApi + ' ' + req.recordId);
        }

        Map<String, Scheduled_Flow__c> exsistingSchedules = new Map<String, Scheduled_Flow__c>();
        for (Scheduled_Flow__c variable : [SELECT Id, Name, Flow_API_Name__c, Record_Id__c, Run_Time__c, Status__c
                                           FROM Scheduled_Flow__c
                                           WHERE Name IN :scheduleNames]) {
            exsistingSchedules.put(variable.name, variable);
        }

        return exsistingSchedules;
    }

    public class Requests {
        @InvocableVariable(label = 'Record Id' description = 'Record Id will be passed to scheduled flow' required = true)
        public ID recordId;

        @InvocableVariable(label = 'Flow API Name' description = 'API name for the flow that will be run on schedule.' required = true)
        public string flowApi;

        @InvocableVariable(label = 'Hours' description = 'How many hours until this flow runs.' required = true)
        public Decimal hours;
    }

    public class Response {
        @InvocableVariable(label = 'Schedule Record' description = 'Scheduled Record that was created for this requeat.')
        public Scheduled_Flow__c record;

        public Response(Scheduled_Flow__c record){
            this.record = record;
        }
    }
}