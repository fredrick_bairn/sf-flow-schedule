@IsTest
public with sharing class FlowSchedularTest {
    @IsTest
    static void shouldScheduleFlow(){
        List<FlowSchedular.Requests> requestList = new List<FlowSchedular.Requests>();
        FlowSchedular.Requests req = new FlowSchedular.Requests();
        req.flowApi = 'Test';
        req.hours = 10;
        req.recordId = '0066e00001WWaAA000';
        requestList.add(req);

        Test.startTest();
        FlowSchedular.scheduleFlow(requestList);
        Test.stopTest();

        Scheduled_Flow__c resultRecord = [SELECT Id, Name, Flow_API_Name__c, Record_Id__c, Run_Time__c, Status__c
                                          FROM Scheduled_Flow__c
                                          LIMIT 1];
        System.assertEquals('Test 0066e00001WWaAA000', resultRecord.name);
        System.assertEquals('Test', resultRecord.Flow_API_Name__c);
        System.assertEquals('0066e00001WWaAA000', resultRecord.Record_Id__c);
        System.assertEquals('Pending', resultRecord.Status__c);

    }


    @IsTest
    static void shouldSchedule(){
        insert new Scheduled_Flow__c(
            Name = 'TEST',
            Flow_API_Name__c = 'test flow',
            Record_Id__c = '0066e00001WWaAA000',
            Run_Time__c = DateTime.now().addDays(-1),
            Status__c = 'Pending',
            Notes__c = ''
            );
        Test.startTest();
        FlowSchedular m = new FlowSchedular();
        m.nextRun();
        Test.stopTest();

    }
}